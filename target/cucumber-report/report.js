$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("features/Test.feature");
formatter.feature({
  "comments": [
    {
      "line": 1,
      "value": "# language: en"
    }
  ],
  "line": 4,
  "name": "Booking",
  "description": "",
  "id": "booking",
  "keyword": "Feature",
  "tags": [
    {
      "line": 3,
      "name": "@Regress"
    }
  ]
});
formatter.scenario({
  "line": 7,
  "name": "Creation of a new booking",
  "description": "",
  "id": "booking;creation-of-a-new-booking",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 6,
      "name": "@Positive"
    },
    {
      "line": 6,
      "name": "@Test"
    }
  ]
});
formatter.step({
  "line": 8,
  "name": "user send \"POST\" http request with parameters to the endpoint \"/booking​\"",
  "rows": [
    {
      "cells": [
        "header",
        "Content-Type",
        "application/json"
      ],
      "line": 9
    },
    {
      "cells": [
        "bodyparam",
        "firstname",
        "John"
      ],
      "line": 10
    },
    {
      "cells": [
        "bodyparam",
        "lastname",
        "Brown"
      ],
      "line": 11
    },
    {
      "cells": [
        "bodyparam",
        "totalprice",
        "111"
      ],
      "line": 12
    },
    {
      "cells": [
        "bodyparam",
        "depositpaid",
        "true"
      ],
      "line": 13
    },
    {
      "cells": [
        "bodyparam",
        "checkin",
        "2018-01-01"
      ],
      "line": 14
    },
    {
      "cells": [
        "bodyparam",
        "checkout",
        "2018-01-04"
      ],
      "line": 15
    },
    {
      "cells": [
        "bodyparam",
        "additionalneeds",
        "Breakfast"
      ],
      "line": 16
    }
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 17,
  "name": "response is received with status code \"200\"",
  "keyword": "Then "
});
formatter.step({
  "line": 18,
  "name": "http response body contains json that corresponds the following scheme: \"src/main/resources/schemes/response/CreateBookingResponse.json\" and with following values",
  "rows": [
    {
      "cells": [
        "firstname",
        "Jim"
      ],
      "line": 19
    },
    {
      "cells": [
        "lastname",
        "brown"
      ],
      "line": 20
    },
    {
      "cells": [
        "totalprice",
        "111"
      ],
      "line": 21
    },
    {
      "cells": [
        "depositpaid",
        "true"
      ],
      "line": 22
    },
    {
      "cells": [
        "checkin",
        "2018-01-01"
      ],
      "line": 23
    },
    {
      "cells": [
        "checkout",
        "2018-01-04"
      ],
      "line": 24
    },
    {
      "cells": [
        "additionalneeds",
        "Breakfast"
      ],
      "line": 25
    },
    {
      "cells": [
        "bookingid",
        "{not null}"
      ],
      "line": 26
    }
  ],
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "POST",
      "offset": 11
    },
    {
      "val": "/booking​",
      "offset": 63
    }
  ],
  "location": "BookingSteps.sendHttpReqWithParameters(String,String,DataTable)"
});
formatter.result({
  "duration": 2481140291,
  "error_message": "cucumber.runtime.CucumberException: No response was received\r\n\tat steps.BookingSteps.sendHttpReqWithParameters(BookingSteps.java:67)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n\tat java.lang.reflect.Method.invoke(Method.java:498)\r\n\tat cucumber.runtime.Utils$1.call(Utils.java:40)\r\n\tat cucumber.runtime.Timeout.timeout(Timeout.java:16)\r\n\tat cucumber.runtime.Utils.invoke(Utils.java:34)\r\n\tat cucumber.runtime.java.JavaStepDefinition.execute(JavaStepDefinition.java:38)\r\n\tat cucumber.runtime.StepDefinitionMatch.runStep(StepDefinitionMatch.java:37)\r\n\tat cucumber.runtime.Runtime.runStep(Runtime.java:300)\r\n\tat cucumber.runtime.model.StepContainer.runStep(StepContainer.java:44)\r\n\tat cucumber.runtime.model.StepContainer.runSteps(StepContainer.java:39)\r\n\tat cucumber.runtime.model.CucumberScenario.run(CucumberScenario.java:44)\r\n\tat cucumber.runtime.junit.ExecutionUnitRunner.run(ExecutionUnitRunner.java:102)\r\n\tat cucumber.runtime.junit.FeatureRunner.runChild(FeatureRunner.java:63)\r\n\tat cucumber.runtime.junit.FeatureRunner.runChild(FeatureRunner.java:18)\r\n\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:290)\r\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:71)\r\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:288)\r\n\tat org.junit.runners.ParentRunner.access$000(ParentRunner.java:58)\r\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:268)\r\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:363)\r\n\tat cucumber.runtime.junit.FeatureRunner.run(FeatureRunner.java:70)\r\n\tat cucumber.api.junit.Cucumber.runChild(Cucumber.java:95)\r\n\tat cucumber.api.junit.Cucumber.runChild(Cucumber.java:38)\r\n\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:290)\r\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:71)\r\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:288)\r\n\tat org.junit.runners.ParentRunner.access$000(ParentRunner.java:58)\r\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:268)\r\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:363)\r\n\tat cucumber.api.junit.Cucumber.run(Cucumber.java:100)\r\n\tat org.junit.runners.Suite.runChild(Suite.java:128)\r\n\tat org.junit.runners.Suite.runChild(Suite.java:27)\r\n\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:290)\r\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:71)\r\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:288)\r\n\tat org.junit.runners.ParentRunner.access$000(ParentRunner.java:58)\r\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:268)\r\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:363)\r\n\tat org.junit.runner.JUnitCore.run(JUnitCore.java:137)\r\n\tat org.junit.runner.JUnitCore.run(JUnitCore.java:115)\r\n\tat org.junit.runner.JUnitCore.runMain(JUnitCore.java:77)\r\n\tat org.junit.runner.JUnitCore.main(JUnitCore.java:36)\r\n\tat launcher.Launcher.main(Launcher.java:21)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n\tat java.lang.reflect.Method.invoke(Method.java:498)\r\n\tat com.intellij.rt.execution.application.AppMain.main(AppMain.java:147)\r\n",
  "status": "failed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 39
    }
  ],
  "location": "BookingSteps.checkResponseStatusCode(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
});