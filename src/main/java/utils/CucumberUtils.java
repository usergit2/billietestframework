package utils;

import configuration.TestUserData;
import constants.ChangeValues;
import enums.HttpParameterLocationEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

/**
 * Created by Rinat on 05.11.2019.
 */
public class CucumberUtils {

    private static Logger logger = LoggerFactory.getLogger(CucumberUtils.class);

    public static void prepareReqBodyValuesToChange(List<String> params, Map<String, String> valuesToChange){
        if(params.get(0).equals(HttpParameterLocationEnum.bodyparam.name())){
            String key = params.get(1);
            String value = params.get(2);
            logger.debug("prepareReqBodyValuesToChange key: " +key +"; value: " + value);
            if(value.contains(ChangeValues.USER_NAME)){
                value = TestUserData.getTestUserName();
            }
            if(value.contains(ChangeValues.PASSWORD)){
                value = TestUserData.getPassword();
            }
            valuesToChange.put(key, value);
        }
    }

    public static void prepareJSONValuesToChange(List<String> params, Map<String, String> valuesToChange){
        String key = params.get(0);
        String value = params.get(1);
        logger.debug("prepareJSONValuesToChange key: " +key +"; value: " + value);
        if(value.contains(ChangeValues.USER_NAME)){
            value = TestUserData.getTestUserName();
        }
        if(value.contains(ChangeValues.PASSWORD)){
            value = TestUserData.getPassword();
        }
        valuesToChange.put(key, value);
    }
}
