package utils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TextNode;
import constants.ChangeValues;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by Rinat on 05.11.2019.
 */
public class TextNodeComparator implements Comparator<JsonNode> {

    private static Logger logger = LoggerFactory.getLogger(TextNodeComparator.class);

    @Override
    public int compare(JsonNode o1, JsonNode o2) {

        logger.debug("o1: " +o1 + "; o2: " + o2);
        if(o1.asText().replaceAll("\n","").trim().equals(ChangeValues.CHECK_FOR_NOT_NULL)){
            return 0;
        }
        if (o1.isArray()) {
            ArrayNode arrayNode = (ArrayNode) o1;
            for (int i = 0; i < arrayNode.size(); i++) {
                compare(o1,o2);
            }
        }
        if (o1.equals(o2)) {
            return 0;
        }
        if(!o1.isValueNode() && !o2.isValueNode()){
            if (o1.isObject()) {
                int result = 1;
                ObjectNode objectNode1 = (ObjectNode) o1;
                ObjectNode objectNode2 = (ObjectNode) o2;
                Iterator<Map.Entry<String, JsonNode>> iter1 = objectNode1.fields();
                Iterator<Map.Entry<String, JsonNode>> iter2 = objectNode2.fields();

                while (iter1.hasNext() && iter2.hasNext()) {
                    result = compare(iter1.next().getValue(), iter2.next().getValue());
                    if(result !=0)
                        return result;
                }
                return result;
            }
        }
        if ((o1 instanceof TextNode) && (o2 instanceof TextNode)) {
            String s1 = ((TextNode) o1).asText();
            String s2 = ((TextNode) o2).asText();
            logger.debug("s1: " +s1+ "\n" + "; s2: " + s2);
            if (s1.equalsIgnoreCase(s2)) {
                return 0;
            }
        }
        return 1;
    }
}
