package utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;

/**
 * Created by Rinat on 02.11.2019.
 */
public class Utils {

    private static Logger logger = LoggerFactory.getLogger(Utils.class);

    private static ObjectMapper mapper = new ObjectMapper();

    public static boolean compareJSONs(String json1, String json2){
        boolean result = false;
        try{
            TextNodeComparator cmp = new TextNodeComparator();
            logger.debug("mapper.readTree(json1): "+mapper.readTree(json1) + "; mapper.readTree(json2): "
                + mapper.readTree(json2));
            result = mapper.readTree(json1).equals(cmp,mapper.readTree(json2));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String setValuesOfJSON(String json, Map<String, String> map){
        try{
            JsonNode root = mapper.readTree(json);
            logger.debug("root node before: " + root);
            setValuesOfJsonScheme("", root, map);
            logger.debug("root node after: " + root);

            json = convertNodeToString(root, mapper);
        }catch (IOException e){
            e.printStackTrace();
        }
        return json;
    }

    private static String convertNodeToString(final JsonNode node, ObjectMapper mapper) throws JsonProcessingException {
        final Object obj = mapper.treeToValue(node, Object.class);
        final String json = mapper.writeValueAsString(obj);
        return json;
    }

    private static void setValuesOfJsonScheme(String currentPath, JsonNode jsonNode, Map<String, String> map) {
        if (jsonNode.isObject()) {
            ObjectNode objectNode = (ObjectNode) jsonNode;
            Iterator<Map.Entry<String, JsonNode>> iter = objectNode.fields();
            while (iter.hasNext()) {
                Map.Entry<String, JsonNode> entry = iter.next();
                String jsonFieldName = entry.getKey();
                String jsonFieldNameValue = map.get(entry.getKey());
                JsonNode node = entry.getValue();
                if(node.isValueNode() && jsonFieldNameValue!=null){
                    logger.debug("map.get(entry.getKey()): " + jsonFieldNameValue
                            + "; entry.getKey(): " + jsonFieldName);
                    if(isInteger(jsonFieldNameValue)) {
                        objectNode.put(jsonFieldName, Integer.valueOf(jsonFieldNameValue));
                    }
                    else if(jsonFieldNameValue.equals("true") || jsonFieldNameValue.equals("false")){
                        objectNode.put(jsonFieldName, Boolean.valueOf(jsonFieldNameValue));
                    }else {
                        objectNode.put(jsonFieldName, jsonFieldNameValue);
                    }
                    logger.debug("objectNode: " + objectNode);
                }
                setValuesOfJsonScheme(jsonFieldName, node, map);
            }
        } else if (jsonNode.isArray()) {
            ArrayNode arrayNode = (ArrayNode) jsonNode;
            for (int i = 0; i < arrayNode.size(); i++) {
                setValuesOfJsonScheme(currentPath, arrayNode.get(i), map);
            }
        }
    }

    public static boolean isInteger(String input) {
        try {
            Integer.parseInt(input);
            return true;
        } catch(NumberFormatException e) {
            return false;
        }
    }
}