package utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Created by Rinat on 05.11.2019.
 */
public class FileReader {

    private static Logger logger = LoggerFactory.getLogger(FileReader.class);

    public static String readFile(File file){
        int c;
        java.io.FileReader reader = null;
        BufferedReader bufferedReader = null;
        String buffer="";
        try {
            bufferedReader = new BufferedReader(reader = new java.io.FileReader(file));
            while((c = bufferedReader.read())!=-1){
                buffer = buffer +(char)c +"";
            }
        }catch (FileNotFoundException e){
            e.printStackTrace();
        }catch(IOException e){
            e.printStackTrace();
        }
        finally {
            try {
                if(bufferedReader!=null){
                    bufferedReader.close();
                }
                if (reader != null) {
                    reader.close();
                }
                logger.debug("Read string = "  + buffer);
                return buffer;
            }catch(IOException e){
                e.printStackTrace();
            }
        }
        return null;
    }
}
