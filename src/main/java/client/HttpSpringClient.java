package client;

import configuration.MainConfiguration;
import constants.ChangeValues;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

public class HttpSpringClient {

    private static Logger logger = LoggerFactory.getLogger(HttpSpringClient.class);

    public static  <T> ResponseEntity<T> sendHttpRequest(String url,
                                                         HttpMethod httpMethod,
                                                         HttpEntity entity,
                                                         Class<T> t) {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<T> response = null;
        logger.debug("url: " + url );
        try {
            response = restTemplate.exchange(url.substring(0,url.length()-1), httpMethod, entity, t);
            logger.debug("HTTP response = " + response.getBody());
        } catch (HttpClientErrorException e){
            logger.error("Request was not sent or didn't receive a response.");
        }
        return response;
    }

    public static String getUri(String url){
        String siteAdress = "";
        if(url.contains(ChangeValues.BOOKING_ID)){
            url.replace(ChangeValues.BOOKING_ID, MainConfiguration.getSiteAdress());
        }
        if(MainConfiguration.getPort() != null && MainConfiguration.getSiteAdress() != null){
            siteAdress = "https://"+ MainConfiguration.getSiteAdress() +":" + MainConfiguration.getPort() +  url;
        }
        if(MainConfiguration.getPort() == null && MainConfiguration.getSiteAdress() != null){
            siteAdress = "https://"+ MainConfiguration.getSiteAdress()  + url;
        }
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(siteAdress.replaceAll("[\r\n]+$", "").trim());
        String uri = builder.build().toUriString();
        logger.info("Generated URL: " + uri+ "; length: " +  uri.toCharArray().length);
        return uri;
    }
}
