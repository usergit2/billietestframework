package configuration;

/**
 * Created by Rinat on 04.11.2019.
 */
public class TestUserData {

    public static String getTestUserName() {
        return System.getProperty("userdata.username");
    }

    public static String getPassword() {
        return System.getProperty("userdata.password");
    }
}
