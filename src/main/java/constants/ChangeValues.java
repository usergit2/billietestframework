package constants;

/**
 * Created by Rinat on 04.11.2019.
 */
public final class ChangeValues {

    public static final String USER_NAME = "{settings:username}";
    public static final String PASSWORD = "{settings:password}";
    public static final String TOKEN = "{testcontext:token}";
    public static final String BOOKING_ID = "{testcontext:id​}";
    public static final String CHECK_FOR_NOT_NULL = "{not null}";
}
