package steps;

import client.HttpSpringClient;
import constants.ChangeValues;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.runtime.CucumberException;
import enums.HttpParameterLocationEnum;
import enums.TestContextKeyEnum;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import utils.CucumberUtils;
import utils.FileReader;
import utils.TestContext;
import utils.Utils;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Rinat on 02.11.2019.
 */
public class BookingSteps {

    private static Logger logger = LoggerFactory.getLogger(BookingSteps.class);

    @Given("^user send \"([^\"]*)\" http request that corresponds the following scheme: \"([^\"]*)\" and with following parameters to the endpoint \"([^\"]*)\"$")
    public void sendHttpReqWithJSONandParameters(String httpMethod, String schemePath, String endpoint, DataTable inputTable) {
        List<List<String>> list = inputTable.raw();
        logger.info("data table: " + list);

        HttpHeaders headers = new HttpHeaders();
        Map<String, String> valuesToChange = new HashMap<>();

        String json = FileReader.readFile(new File(schemePath));
        logger.debug("json pattern: " + json);
        for(List<String> params: list){

            if(params.get(0).equals(HttpParameterLocationEnum.header.name())){
                String key = params.get(1);
                String value = params.get(2);
                if (value.contains(ChangeValues.TOKEN)){
                    value = TestContext.get(TestContextKeyEnum.token);
                }
                logger.debug("Http request header parameters. Key: " +key +"; value: " + value);
                headers.add(key,value);
            }
            CucumberUtils.prepareReqBodyValuesToChange(params,valuesToChange);
        }
        HttpEntity<String> entity = new HttpEntity<>(Utils.setValuesOfJSON(json,valuesToChange),headers);

        logger.debug("HTTP request header: " + entity.getHeaders());
        logger.debug("HTTP request body: " + entity.getBody());
        ResponseEntity<String> response = HttpSpringClient
                .sendHttpRequest(
                        HttpSpringClient.getUri(endpoint),
                        HttpMethod.resolve(httpMethod),
                        entity,
                        String.class);
         if(response == null){
            throw new CucumberException("No response was received");
        }
        TestContext.put(TestContextKeyEnum.responseStatus, response.getStatusCode());
        TestContext.put(TestContextKeyEnum.reponseBody, response.getBody());
    }

    @Given("^user send \"([^\"]*)\" http request with following parameters to the endpoint \"([^\"]*)\"$")
    public void sendHttpReqWithParameters(String httpMethod, String endpoint, DataTable inputTable) {
        List<List<String>> list = inputTable.raw();
        logger.info("data table: " + list);

        HttpHeaders headers = new HttpHeaders();
        Map<String, String> valuesToChange = new HashMap<>();

        for(List<String> params: list){

            if(params.get(0).equals(HttpParameterLocationEnum.header.name())){
                String key = params.get(1);
                String value = params.get(2);
                if (value.contains(ChangeValues.TOKEN)){
                    value = TestContext.get(TestContextKeyEnum.token);
                }
                logger.debug("Http request header parameters. Key: " +key +"; value: " + value);
                headers.add(key,value);
            }
            CucumberUtils.prepareReqBodyValuesToChange(params,valuesToChange);
        }
        HttpEntity<String> entity = new HttpEntity<>(headers);

        logger.debug("HTTP request header: " + entity.getHeaders());
        logger.debug("HTTP request body: " + entity.getBody());
        ResponseEntity<String> response = HttpSpringClient
                .sendHttpRequest(
                        HttpSpringClient.getUri(endpoint),
                        HttpMethod.resolve(httpMethod),
                        entity,
                        String.class);
        if(response == null){
            throw new CucumberException("No response was received");
        }
        TestContext.put(TestContextKeyEnum.responseStatus, response.getStatusCode());
        TestContext.put(TestContextKeyEnum.reponseBody, response.getBody());
    }


        @Then("^response is received with status code \"([^\"]*)\"$")
    public void checkResponseStatusCode(String statusCode){
        Assert.assertNotNull("Http response code is absent in Text context"
                ,TestContext.get(TestContextKeyEnum.responseStatus));
        int responseCode = TestContext.get(TestContextKeyEnum.responseStatus);
        logger.debug("Response code: " + responseCode);
        Assert.assertTrue("Response code is different from the entered one",
                responseCode == Integer.valueOf(statusCode));
    }

    @And("^http response body contains json that corresponds the following scheme: \"([^\"]*)\"$")
    public void checkResponseBodyJson(String schemeAddress, DataTable inputTable){
        Assert.assertNotNull("Address of the scheme is not provided", schemeAddress.equals(""));
        Assert.assertNotNull("Http response body is absent in Text context"
                ,TestContext.get(TestContextKeyEnum.reponseBody));
        Assert.assertNotNull("Http response body is absent in Text context"
                ,TestContext.get(TestContextKeyEnum.reponseBody));
        List<List<String>> list = inputTable.raw();
        logger.info("data table: " + list);
        Map<String, String> valuesToChange = new HashMap<>();

        String json = FileReader.readFile(new File(schemeAddress));
        logger.debug("json pattern: " + json);
        for(List<String> params: list) {
            CucumberUtils.prepareJSONValuesToChange(params,valuesToChange);
        }
        String expectedJSON = Utils.setValuesOfJSON(json,valuesToChange);
        String receivedJSON = TestContext.get(TestContextKeyEnum.reponseBody);
        logger.debug("expectedJSON: " +"\n" + expectedJSON);
        logger.debug("receivedJSON: " +"\n" + receivedJSON);
        Assert.assertTrue("JSONs are not identical", Utils.compareJSONs(expectedJSON,receivedJSON
                .replaceAll("\n","").trim()));
    }
}
