# language: en

@Regress
Feature: Booking

  @Positive @Test
  Scenario: Creation of a new booking
     Given user send "POST" http request that corresponds the following scheme: "src/main/resources/schemes/request/CreateBookingRequest.json" and with following parameters to the endpoint "/booking​"
       | header     | Content-Type    | application/json |
       | bodyparam  | firstname       | John             |
       | bodyparam  | lastname        | Brown            |
       | bodyparam  | totalprice      | 111              |
       | bodyparam  | depositpaid     | true             |
       | bodyparam  | checkin         | 2018-01-01       |
       | bodyparam  | checkout        | 2018-01-04       |
       | bodyparam  | additionalneeds | Breakfast        |
     Then response is received with status code "200"
     And http response body contains json that corresponds the following scheme: "src/main/resources/schemes/response/CreateBookingResponse.json" and with following values
       | firstname       | Jim              |
       | lastname        | brown            |
       | totalprice      | 111              |
       | depositpaid     | true             |
       | checkin         | 2018-01-01       |
       | checkout        | 2018-01-04       |
       | additionalneeds | Breakfast        |
       | bookingid       | {not null}       |


  @Positive
  Scenario: Amendment of created booking
     Given user send "POST" http request that corresponds the following scheme: "src/main/resources/schemes/request/CreateBookingRequest.json" and with following parameters to the endpoint "/booking​"
       | header     | Content-Type    | application/json |
       | bodyparam  | firstname       | Jim              |
       | bodyparam  | lastname        | Brown            |
       | bodyparam  | totalprice      | 111              |
       | bodyparam  | depositpaid     | true             |
       | bodyparam  | checkin         | 2018-01-01       |
       | bodyparam  | checkout        | 2018-01-04       |
       | bodyparam  | additionalneeds | Breakfast        |
     Then response is received with status code "200"
     And http response body contains json that corresponds the following scheme: "src/main/resources/schemes/response/CreateBookingResponse.json" and with following values
	   | firstname       | Jim              |
       | lastname        | Brown            |
       | totalprice      | 111              |
       | depositpaid     | true             |
       | checkin         | 2018-01-01       |
       | checkout        | 2018-01-04       |
       | additionalneeds | Breakfast        |
       | bookingid       | {not null}       |
     Given user send "POST" http request that corresponds the following scheme: "src/main/resources/schemes/request/CreateTokenRequest.json" and with following parameters to the endpoint "/auth"
       | header     | Content-Type | application/json    |
       | bodyparam  | username     | {settings:username} |
       | bodyparam  | password     | {settings:password} |
     Then response is received with status code "200"
     And http response body contains json that corresponds the following scheme: "src/main/resources/schemes/response/CreateTokenResponse.json" and with following values
       | token | {not null} |
     Given user send "PUT" http request that corresponds the following scheme: "src/main/resources/schemes/request/UpdateBookingRequest.json" and with following parameters to the endpoint "/booking/{testcontext:id​}"
       | header     | Content-Type    | application/json    |
       | header     | Accept          | application/json    |
       | header     | Cookie          | {testcontext:token} |
       | bodyparam  | checkin         | 2018-01-02          |
       | bodyparam  | checkout        | 2018-01-08          |
     Then response is received with status code "200"
     And http response body contains json that corresponds the following scheme: "src/main/resources/schemes/response/UpdateBookingResponse.json" and with following values
       | field           | value      |
       | firstname       | Jim        |
       | lastname        | Brown      |
       | totalprice      | 111        |
       | depositpaid     | true       |
       | checkin         | 2018-01-02 |
       | checkout        | 2018-01-08 |
       | additionalneeds | Breakfast  |

  @Positive
  Scenario: Getting booking by id
     Given user send "POST" http request that corresponds the following scheme: "src/main/resources/schemes/request/CreateBookingRequest.json" and with following parameters to the endpoint "/booking​"
       | header     | Content-Type    | application/json |
       | bodyparam  | firstname       | Jim              |
       | bodyparam  | lastname        | Brown            |
       | bodyparam  | totalprice      | 111              |
       | bodyparam  | depositpaid     | true             |
       | bodyparam  | checkin         | 2018-01-01       |
       | bodyparam  | checkout        | 2018-01-04       |
       | bodyparam  | additionalneeds | Breakfast        |
     Then response is received with status code "200"
     And http response body contains json that corresponds the following scheme: "src/main/resources/schemes/response/CreateBookingResponse.json" and with following values
	   | firstname       | Jim              |
       | lastname        | Brown            |
       | totalprice      | 111              |
       | depositpaid     | true             |
       | checkin         | 2018-01-01       |
       | checkout        | 2018-01-04       |
       | additionalneeds | Breakfast        |
       | bookingid       | {not null}       |
     Given user send "GET" http request with following parameters to the endpoint "/booking/{testcontext:id​}"
       | header     | Accept          | application/json |
     Then response is received with status code "200"
     And http response body contains json that corresponds the following scheme: "src/main/resources/schemes/response/GetBookingResponse.json" and with following values
	   | firstname       | Jim              |
       | lastname        | Brown            |
       | totalprice      | 111              |
       | depositpaid     | true             |
       | checkin         | 2018-01-01       |
       | checkout        | 2018-01-04       |
       | additionalneeds | Breakfast        |

  @Positive
  Scenario: Delete booking
     Given user send "POST" http request that corresponds the following scheme: "src/main/resources/schemes/request/CreateBookingRequest.json" and with following parameters to the endpoint "/booking​"
       | header     | Content-Type    | application/json |
       | bodyparam  | firstname       | Jim              |
       | bodyparam  | lastname        | Brown            |
       | bodyparam  | totalprice      | 111              |
       | bodyparam  | depositpaid     | true             |
       | bodyparam  | checkin         | 2018-01-01       |
       | bodyparam  | checkout        | 2018-01-04       |
       | bodyparam  | additionalneeds | Breakfast        |
     Then response is received with status code "200"
     And http response body contains json that corresponds the following scheme: "src/main/resources/schemes/response/CreateBookingResponse.json" and with following values
	   | firstname       | Jim              |
       | lastname        | Brown            |
       | totalprice      | 111              |
       | depositpaid     | true             |
       | checkin         | 2018-01-01       |
       | checkout        | 2018-01-04       |
       | additionalneeds | Breakfast        |
       | bookingid       | {not null}       |
     Given user send "POST" http request that corresponds the following scheme: "src/main/resources/schemes/request/CreateTokenRequest.json" and with following parameters to the endpoint "/auth"
       | header     | Content-Type | application/json    |
       | bodyparam  | username     | {settings:username} |
       | bodyparam  | password     | {settings:password} |
     Then response is received with status code "200"
     And http response body contains json that corresponds the following scheme: "src/main/resources/schemes/response/CreateTokenResponse.json" and with following values
       | token | {not null} |
     Given user send "DELETE" http request with following parameters to the endpoint "/booking/{testcontext:id​}"
       | header     | Content-Type    | application/json    |
       | header     | Cookie          | {testcontext:token} |
     Then response is received with status code "201"
     Given user send "GET" http request with following parameters to the endpoint "/booking/{testcontext:id​}"
       | header     | Accept          | application/json |
     Then response is received with status code "404"