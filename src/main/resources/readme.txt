Project can be started via Launcher class. Desireable test can be marked by corresponding annotation in feature file and
Launcher class.
Input VM parameters: -Dsite.adress=restful-booker.herokuapp.com -Dallure.results_pattern=allure-results
Patterns for output/input json can be of any type and must be provided before hand as a text file.
IN this particular program outputs are situated in src/main/resources/schemes/request and inputs src/main/resources/schemes/response

Values for request and response must be provided in the corresponding cucumber steps.
Logging properties are set in log4.properties file.
Symbols aka {testcontext:id​} means that this value is taken from the text context by key value: "id".
Symbols aka {not null} means that during json comparison this field will not be compared to the other value except that it exist.